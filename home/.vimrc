" vim: set foldmarker={,} foldlevel=0 foldmethod=marker spell:
set nocompatible

" Set up plugins {
filetype off                   " required!
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" Load the bundles list
if filereadable(expand("~/.vimrc.bundles"))
    source ~/.vimrc.bundles
endif

set rtp+=/opt/packages/powerline/powerline/bindings/vim

filetype plugin indent on     " required!
" }

" Formatting {
set tabstop=4 
set shiftwidth=4    " Indents will have a width of 4
set softtabstop=4   " Sets the number of columns for a TAB
set expandtab       " Expand TABs to spaces

" }

" UI options {
" Color options 
let g:solarized_termtrans=1
let g:solarized_contrast="high"
let g:solarized_visibility="high"
color solarized " Load a colorscheme
syntax enable
set background=dark

set number

" Powerline
set laststatus=2
set noshowmode

set splitright " Puts new vsplit windows to the right of the current
set splitbelow " Puts new split windows to the bottom of the current
" }

" Key mappings {
    " Disable the Arrow keys in normal and insert mode {
    no <down> <Nop>
    no <left> <Nop>
    no <right> <Nop>
    no <up> <Nop>

    ino <down> <Nop>
    ino <left> <Nop>
    ino <right> <Nop>
    ino <up> <Nop>
    " }
" }


